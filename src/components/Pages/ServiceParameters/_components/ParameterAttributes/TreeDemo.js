import React from "react";
import TreeView from "@material-ui/lab/TreeView";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import TreeItem from "@material-ui/lab/TreeItem";

import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";

const useTreeItemStyles = makeStyles((theme) => ({
    root: {
        "&:focus > $content $label": {
            // backgroundColor: "blue",
        },
    },
    content: {},
    group: {
        marginLeft: 0,
        "& $content": {
            paddingLeft: theme.spacing(2),
        },
    },
    expanded: {},
    selected: {
        // backgroundColor: "red",
    },
    label: {
        fontWeight: "inherit",
        color: "inherit",
    },
    labelRoot: {
        display: "flex",
        alignItems: "center",
        padding: theme.spacing(0.5, 0),
    },
    labelText: {
        fontWeight: "inherit",
        flexGrow: 1,
    },
}));

function StyledTreeItem(props) {
    const classes = useTreeItemStyles();
    const { labelText, labelIcon: LabelIcon, labelInfo, color, bgColor, ...other } = props;

    return (
        <TreeItem
            label={
                <>
                    <ListItem className={classes.labelRoot}>
                        <Typography variant="body2" className={classes.labelText}>
                            {labelText}
                        </Typography>
                    </ListItem>
                </>
            }
            classes={{
                root: classes.root,
                content: classes.content,
                expanded: classes.expanded,
                selected: classes.selected,
                group: classes.group,
                label: classes.label,
            }}
            {...other}
        />
    );
}

export default function MultiSelectTreeView() {
    return (
        <TreeView defaultCollapseIcon={<ExpandMoreIcon />} defaultExpandIcon={<ChevronRightIcon />} multiSelect>
            <StyledTreeItem nodeId="1" labelText="Term_types">
                <StyledTreeItem nodeId="1.1" labelText="Status" />
                <StyledTreeItem nodeId="1.2" labelText="Stamp_user" />
                <StyledTreeItem nodeId="1.3" labelText="Status_reason" />
            </StyledTreeItem>
        </TreeView>
    );
}
