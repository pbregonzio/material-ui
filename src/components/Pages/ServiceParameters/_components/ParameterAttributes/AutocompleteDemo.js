import React from "react";
import Chip from "@material-ui/core/Chip";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";

export default function Tags({ data }) {
    return (
        <Autocomplete
            multiple
            id="tags-outlined"
            options={data.map((option) => option.name)}
            defaultValue={[data[3].name]}
            disableCloseOnSelect
            limitTags={2}
            fullWidth
            renderOption={(option, { selected }) => <>{option}</>}
            renderInput={(params) => (
                <TextField {...params} variant="outlined" label="filterSelectedOptions" placeholder="Favorites" />
            )}
            renderTags={(value, getTagProps) =>
                value.map((option, index) => <Chip label={option} {...getTagProps({ index })} />)
            }
        />
    );
}
